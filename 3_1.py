def symbols_in_words():
    s = input().lower()
    a = 'g'
    b = 'c'
    count = 0
    for i in range(len(s)):
        if a == s[i] or b == s[i]:
            count += 1

    result = count/len(s) * 100
    return result

print(symbols_in_words())
