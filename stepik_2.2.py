def read_numbers():
   while(True):
       number = int(input())
       if number <= 100 and number >= 10:
           print(number)
       elif number < 10:
           continue
       else:
           break
read_numbers()