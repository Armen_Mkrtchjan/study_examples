print("Добро пожаловать в программу Рекорды!")
choice = None
scores = []
while choice != '0':
    print("0 - Выйти из программы \n1 - Показать рекорды \n2 - Добавить рекорд\n3 - Удалить рекорд")
    choice = input("Ваш выбор: ")
    if choice == '0':
        print("До свидания!")
        break
    elif choice == '1':
        print("Рекорды \n")
        print("ИМЯ\tРЕЗУЛЬТАТ")
        for entry in scores:
            scores.sort(reverse=True)
            score, name = entry
            print(name, '\t', score)
            print()
    elif choice == '2':
        name = input("Введите имя игрока: ")
        score = int(input("Введите результат: "))
        new_entry = (score, name)
        scores.append(new_entry)
        scores.sort(reverse=True)
        scores = scores[:5]
    elif choice == '3':
        del_choice = input("Введите рекорд для удаления: ")
        for score in scores:
            if del_choice in score:
                d = scores.index(score)
                scores.remove(score)
            else:
                print("Рекорд не найден!")
    else:
        print("Извините, в меню нет пункта ", choice)
